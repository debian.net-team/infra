#!/usr/bin/env python3

from pathlib import Path
import re

from yaml import safe_load


def guess_usernames(owners):
    for owner in owners:
        m = re.search(r'<(.*)@debian\.org>', owner)
        if m:
            yield m.group(1)

def generate_line(meta):
    machine = meta['machine']
    hostname = machine['hostname']
    usernames = [f'@{user}' for user in guess_usernames(machine['owners'])]
    return f' * [ ] {hostname}: {", ".join(usernames)}'


def main():
    p = Path('vms')
    for vm in p.iterdir():
        with vm.open() as f:
            meta = safe_load(f)
            print(generate_line(meta))


if __name__ == '__main__':
    main()
